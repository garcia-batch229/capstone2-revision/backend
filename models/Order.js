const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
   userId: {
      type: String,
      required: true
   },
   username: {
      type: String,
      required: true
   },
   email: {
      type: String,
      required: true
   },
   products: [
      {
         productId: {
            type: String,
            required: true,
         },
         productName: {
            type: String,
            required: true
         },
         quantity: {
            type: Number,
            required: true
         },
         price: {
            type: Number,
            required: true
         }         
      },
   ],
   totalAmount: {
      type: Number,
      required: true
   },
   purchasedOn: {
      type: String,
      default: new Date()
   },
   isActive: {
      type: String,
      default: true
   }
});

module.exports = mongoose.model("Order", orderSchema);
