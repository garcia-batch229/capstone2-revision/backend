const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	username: {
      type: String,
      required: [true, "Username is required"]
   },
   firstName: {
      type: String,
      required: [true, "firstName is required"]
   },
   lastName: {
      type: String,
      required: [true, "lastName is required"]
   },
   email: {
      type: String,
      required: [true, "Email is required"]
   },
   password: {
      type: String,
      required: [true, "Password is required"]
   },
   isAdmin: {
      type: Boolean,
      default: false
   },
   createdOn: {
      type: String,
      default: new Date()
   }
});


module.exports = mongoose.model("User", userSchema);
