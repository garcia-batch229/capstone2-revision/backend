// modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Local modules
//const User = require("../models/User");
const Product = require("../models/Product");
//const Order = require("../models/Order");
const auth = require("../auth");


////////////////////////////////////
//Register a product (Admin Account)
////////////////////////////////////
module.exports.addProduct = (reqBody) => {
	let product = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		image: reqBody.image,
	})
	return Product.findOne({name: reqBody.name}).then((result) => {
		if(result != null){
			return false;
		}else {
			return product.save().then((result, error) => {
				if(error){
					return false;
				}else{
					return true;
				}
			})
		}
	})
}
///////////////////
//Show all products
///////////////////
module.exports.allProduct = () => {
	return Product.find().then((result) => {
		return result;
	})
}

///////////////////////////////////////////////////////////
//Change the status from Active to Inactive (Admin Account)
///////////////////////////////////////////////////////////
module.exports.setInActive = (reqParams) => {
	
		let status = {
			isActive: false
		}		
		return Product.findByIdAndUpdate(reqParams,status).then((result, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})		
}

///////////////////////////////////////////////////////////
//Change the status from Inactive to Active (Admin Account)
///////////////////////////////////////////////////////////
module.exports.setActive = (reqParams) => {
	let status = {isActive: true}
	return Product.findByIdAndUpdate(reqParams, status).then((result,error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

///////////////////////////
//Retrieve a single product
///////////////////////////
module.exports.specificProduct = (reqBody) => {
	return Product.findById({_id: reqBody._id}).then((result) => {
		return result;
	})
}


/////////////////////////////////
// Update product (Admin Account)
/////////////////////////////////
module.exports.productUpdate = (reqBody,reqParams) => {
	let productData = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		image: reqBody.image
	}
	return Product.findByIdAndUpdate(reqParams,productData).then((result,error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


/////////////////////
// Get active product 
/////////////////////
module.exports.getActiveProduct = () => {
	return Product.find({isActive: true}).then((result) => {
		return result;
	})
}