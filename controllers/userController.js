// modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Local modules
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const auth = require("../auth");

//Register a user
module.exports.register = (reqBody) => {
	let user = new User({
		username: reqBody.username,
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10)
	})
	//Check if email already exists
	return User.findOne({username: user.username}).then((result) => {
		if(result != null){
			return false;
		}else{
			return User.findOne({email:user.email}).then((result) => {
				if(result != null){
					return false;
				}else{
					return user.save().then((result,error) => {
						if(error){
							return false;
						}else{
							return true;
						}
					})
				}
			})
			
		}
	})
}

//////////////////////////////////
//Login user (create access token)
//////////////////////////////////
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
}

////////////////////////////
//Retrieve all users (Admin Account)
////////////////////////////
module.exports.allUser = () => {
	return User.find().then(result => {
		return result;
	})
}

/////////////////////////////////
//Retrieve all admin users (Admin Account)
//////////////////////////////////
module.exports.getAdmin = () => {

	return User.find({isAdmin: true}).then(result => {
		return result;
	})
}

///////////////////////
//Retrieve user details
///////////////////////
module.exports.userDetails = (data) => {
	
	return User.findById(data).then(result => {
		return result;
	})
}

/////////////////////////////
//Change User status to Admin
/////////////////////////////
module.exports.setAsAdmin = (reqParams) => {
	let status = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(reqParams, status).then((result, error) => {
		if(error){
			return false;
		}else {
			return true;
		}
	})
}

/////////////////////////////
//Change Admin to User status
/////////////////////////////
module.exports.setAsUser = (reqParams) => {
	let status = {isAdmin: false}
	return User.findByIdAndUpdate(reqParams, status).then((result, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}