const express = require("express");
const Order = require("../models/Order.js");
const User = require("../models/User.js");
const Product = require("../models/Product.js");

/////////////////////////////
// Place order (User Account)
/////////////////////////////
module.exports.placeOrder =  (data) => {

	return User.findOne({username: data.username}).then((userResult) => {
		
		if(userResult != null){
			return	Product.findOne({name: data.reqBody.products.productName}).then((productResult) => {
				console.log(productResult);
				let order = new Order({
					userId: data.id,
					username: data.username,
					email: userResult.email,
					products: {
						productId: productResult._id,
						productName: productResult.name,
						quantity: data.reqBody.products.quantity,
						price: productResult.price,
						},					
					totalAmount: (data.reqBody.products.quantity * productResult.price)
				})
				console.log(order);
				return order.save().then((result, error) => {
					if(error){
						return false;
					}else{
						return true;
					}
				})
			})

		}else{
			return false;
		}
	})

}

////////////////////////////////
//Get All Orders (Admin Account)
////////////////////////////////
module.exports.getOrder = () => {
	return Order.find().then((result) => {
		return result;
	})
}


/////////////////////
//Get Specific Orders 
/////////////////////
module.exports.myOrder = (data) => {
	console.log(data);
	return Order.find({userId:data}).then((result) => {
		return	result;
	})
}

/////////////////////////////
//Cancel Order (User Account)
/////////////////////////////
module.exports.cancelOrder = (reqParams) => {
	return Order.findByIdAndRemove({_id: reqParams}).then((result,error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}