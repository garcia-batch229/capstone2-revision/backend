const express = require("express");
const jwt = require("jsonwebtoken");
const auth = require("../auth.js");

const router = express.Router();
const userControllers = require("../controllers/userController");
///////////////
//Register User
///////////////
router.post("/register", (req,res) => {
	userControllers.register(req.body).then((resultFromController) => res.send(resultFromController)
	)
})

////////////
//Login User
////////////
router.post("/login", (req,res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})
////////////////////////////////////
//Retrieve all users (Admin Account)
////////////////////////////////////
router.get("/", auth.verify, (req,res) => {
	const data = auth.decode(req.headers.authorization);
	if(data.isAdmin){
		userControllers.allUser().then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false);
	}
})
////////////////////////////////
//Get Admin user (Admin Account)
////////////////////////////////
router.get("/admin", auth.verify, (req,res) => {
	const data = auth.decode(req.headers.authorization);
	if(data.isAdmin){
		userControllers.getAdmin().then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false);
	}
})
//////////////////////////////////
//Get User Details (Admin Account)
//////////////////////////////////
router.get("/details", auth.verify, (req,res) => {
	const userData =  auth.decode(req.headers.authorization).id
	
	console.log(userData);
	userControllers.userDetails(userData).then((resultFromController) => res.send(resultFromController))
})
///////////////////////////////////////////////
//Change a User to Admin status (Admin Account)
///////////////////////////////////////////////
router.put("/setAdmin/:userId", auth.verify, (req,res) => {
	const data = auth.decode(req.headers.authorization)
	if(data.isAdmin){

		userControllers.setAsAdmin(req.params.userId).then((resultFromController) => res.send(resultFromController));
	}else{
		res.send(false);
	}
})

////////////////////////////////
//Change an Admin to User status
////////////////////////////////
router.put("/setUser/:userId", auth.verify, (req,res) => {
	const data = auth.decode(req.headers.authorization)
	if(data.isAdmin){
		userControllers.setAsUser(req.params.userId).then((resultFromController) => res.send(resultFromController));
	}else{
		return false;
	}
	
})

















module.exports = router;