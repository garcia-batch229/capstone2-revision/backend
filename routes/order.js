const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const jwt = require("jsonwebtoken");
const orderController = require("../controllers/orderController.js")	



/////////////////////////////
// Place order (User Account)
/////////////////////////////
router.post("/placeOrder", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		username: auth.decode(req.headers.authorization).username,
		id: auth.decode(req.headers.authorization).id,
		reqBody: req.body
	}
	console.log(data);
	if(data.isAdmin == false){
		orderController.placeOrder(data).then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false)
	}
})

////////////////////////////////
//Get All Orders (Admin Account)
////////////////////////////////
router.get("/", auth.verify, (req,res) => {
	const data = auth.decode(req.headers.authorization).isAdmin;
	if(data){
		orderController.getOrder().then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false);
	}
	
})

/////////////////////
//Get Specific Orders (User Account)
/////////////////////
router.get("/myOrder", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		id: auth.decode(req.headers.authorization).id
	}
	
	if(data.isAdmin == false){
		orderController.myOrder(data.id).then((resultFromController) => res.send(resultFromController));
	}else{
		res.send(false);
	}
	
})

/////////////////////////////
//Cancel Order (User Account)
/////////////////////////////
router.delete("/cancelOrder/:orderId", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
			}
			console.log(data);
	if(data.isAdmin == false){
		orderController.cancelOrder(req.params.orderId).then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false);
	}
})




module.exports = router;