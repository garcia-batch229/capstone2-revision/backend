const express = require("express");
const jwt = require("jsonwebtoken");
const auth = require("../auth.js");
const productControllers = require("../controllers/productController")
const router = express.Router();

///////////////////////////////
//Add a product (Admin Account)
///////////////////////////////
router.post("/addProduct", auth.verify, (req,res) => {
	let data = auth.decode(req.headers.authorization).isAdmin;
	if(data){
		productControllers.addProduct(req.body).then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false);
	}
})

//////////////////
//Show all product.
//////////////////
router.get("/", (req,res) => {
	productControllers.allProduct().then((resultFromController) => res.send(resultFromController))
})


///////////////////////////////////////////////////////////
//Change the status from Active to Inactive (Admin Account)
///////////////////////////////////////////////////////////
router.put("/setInActive/:productId", auth.verify, (req,res) => {
	const data = auth.decode(req.headers.authorization).isAdmin;
	
	if(data){
		productControllers.setInActive(req.params.productId).then((resultFromController) => res.send(resultFromController));
	}else{
		res.send(false);
	}
})


///////////////////////////////////////////////////////////
//Change the status from Inactive to Active (Admin Account)
///////////////////////////////////////////////////////////
router.put("/setActive/:productId", auth.verify, (req,res) => {
	const data = auth.decode(req.headers.authorization).isAdmin;
	if(data){
		productControllers.setActive(req.params.productId).then((resultFromController) => res.send(resultFromController));
	}else{
		res.send(false);
	}
})

///////////////////////////
//Retrieve a single product
///////////////////////////

router.get("/specificProduct", (req,res) => {
	productControllers.specificProduct(req.body).then((resultFromController) => res.send(resultFromController))
})

///////////////////////////////////////////////
// Update product by admin only (Admin Account)
///////////////////////////////////////////////
router.put("/productUpdate/:productId", auth.verify, (req,res) => {
	const data = auth.decode(req.headers.authorization).isAdmin;
	if(data){
		productControllers.productUpdate(req.body, req.params.productId).then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false);
	}
})

/////////////////////
// Get active product 
/////////////////////
router.get("/getActiveProduct", (req,res) => {
	productControllers.getActiveProduct().then((resultFromController) => res.send(resultFromController));
})




module.exports = router;